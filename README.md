Built following [Jason's tutorial](https://git.satanic.cloud/jcolyer/how-tos/blob/master/gitlab-high-availability/setup-ha.md)

# How to access the containers

1) `docker-compose up -d`
2) `docker exec -it <name of container in docker-compose.yml> bash`
   * Example: `docker exec -it consul1 bash`

# Troubleshooting

You might run into issues while running `sudo gitlab-ctl reconfigure`; namely having it freeze at a recipe. You can fix that by running:

1) `sudo cp /opt/gitlab/embedded/cookbooks/runit/files/default/gitlab-runsvdir.conf /etc/init/` (or /`etc/init.d if you don't have an /init`)
2) `sudo /opt/gitlab/embedded/bin/runsvdir-start`
    The output might be:
    
    ```/opt/gitlab/embedded/bin/runsvdir-start: line 24: ulimit: pending signals: cannot modify limit: Operation not permitted /opt/gitlab/embedded/bin/runsvdir-start: line 37: /proc/sys/fs/file-max: Read-only file system```
    
You can run `cntrl c` and then `sudo gitlab-ctl status` to confirm that the service is running
3) Repeat step 2 until all services are up and Chef doesn't freeze 


    